<h1 align="center">三方组件资源汇总</h1> 

##### 本文收集了一些已经开源的三方组件资源，欢迎应用开发者参考和使用，同时也欢迎开发者贡献自己的开源组件库，可以提PR加入到列表当中

## 目录

- [工具](#工具)
- [三方组件JS_ArkTS](#三方组件JS_ArkTS)
    - [UI](#ui-自定义控件JS_ArkTS)
        - [Image](#image-JS_ArkTS)
        - [Text](#text-JS_ArkTS)
        - [ListView](#listview-JS_ArkTS)
        - [Indicator](#indicator-JS_ArkTS)
        - [PageSlider](#pageslider-JS_ArkTS)
        - [ProgressBar](#progressbar-JS_ArkTS)
        - [Dialog](#dialog-JS_ArkTS)
        - [Layout](#layout-JS_ArkTS)
        - [Chips](#chips-JS_ArkTS)
        - [Tab-菜单切换](#tab-菜单切换JS_ArkTS)
        - [选择器](#选择器JS_ArkTS)
        - [其他UI-自定义控件](#其他ui-自定义控件JS_ArkTS)
    - [动画](#动画JS_ArkTS)
        - [动画](#动画JS_ArkTS)
    - [图片](#图片JS_ArkTS)
        - [图片加载](#图片加载JS_ArkTS)
        - [图片处理](#图片处理JS_ArkTS)
    - [多媒体](#多媒体JS_ArkTS)
    - [文件数据](#文件数据JS_ArkTS)
        - [数据库](#数据库JS_ArkTS)
        - [数据存储](#数据存储JS_ArkTS)
        - [数据封装传递](#数据封装传递JS_ArkTS)
        - [文件解析编码解码](#文件解析编码解码JS_ArkTS)
    - [网络](#网络-JS_ArkTS)
        - [网络](#网络JS_ArkTS)
    - [安全](#安全-JS_ArkTS)
        - [加密算法](#加密算法JS_ArkTS)
    - [工具](#工具JS_ArkTS)
        - [相机-相册](#相机-相册JS_ArkTS)
        - [日志](#日志JS_ArkTS)
        - [蓝牙工具](#蓝牙工具JS_ArkTS)
    - [其他](#其他JS_ArkTS)
        - [其他](#其他JS_ArkTS)
- [三方组件C_CPP](#三方组件C_CPP)
    - [工具类](#工具类C_CPP)
        - [音视频](#音视频C_CPP)
        - [加解密算法](#加解密算法C_CPP)
        - [图像处理](#图像处理C_CPP)
        - [网络通信](#网络通信C_CPP)
        - [数据压缩](#数据压缩C_CPP)
        - [XML解析](#XML解析C_CPP)
        - [编码转换](#编码转换C_CPP)
        - [其他工具类](#其他工具类C_CPP)

## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio

[返回目录](#目录)

## <a name="三方组件JS_ArkTS"></a>三方组件JS_ArkTS

### <a name="ui-自定义控件JS_ArkTS"></a>UI

#### <a name="image-JS_ArkTS"></a>Image

- [subsampling-scale-image-view](https://gitee.com/openharmony-sig/subsampling-scale-image-view) - 视图缩放组件
- [RoundedImageView](https://gitee.com/openharmony-sig/RoundedImageView) - 圆角图片设置组件
- [MaterialProgress](https://gitee.com/applibengineering/MaterialProgress) 
- [CircleImageView](https://gitee.com/openharmony-sig/CircleImageView) - 自定义圆形imageview，主要实现圆形图片展示
- [PhotoView](https://gitee.com/openharmony-sig/PhotoView) - 图片加载、缩放、浏览组件
- [NineGridImageViewJS](https://gitee.com/openharmony-tpc/NineGridImageViewJS) - 九宫格图片展示
- [vant](https://gitee.com/vant-openharmony/vant) - 轻量、可靠的 openharmony UI 组件库，提供多种icon设计
- [OpenHarmony-JS-Icon](https://gitee.com/chenqiao002/open-harmony-js-icon) - 通过使用常用组件、画布组件和自定义组件等来实现一个自定义的icon组件
- [ContinuousScrollableImageJS](https://gitee.com/openharmony-tpc/ContinuousScrollableImageJS) - 带动画播放的Image
- [RoundImage](https://gitee.com/applibgroup/RoundImage) - This library is developed to provide rounded image corners and oval shaped image
- [MaterialSelect](https://gitee.com/applibengineering/MaterialSelect) - This library is developed to provide select functionality from list of options
- [MaterialLists](https://gitee.com/applibgroup/MaterialLists)-This library is developed to provide UI of different type of lists  for given data
- [MaterialSwitch](https://gitee.com/applibgroup/MaterialSwitch) - This library is developed to provide customizable toggle switches
[返回目录](#目录)

#### <a name="text-JS_ArkTS"></a>Text

- [ohos-autofittextview](https://gitee.com/openharmony-sig/ohos-autofittextview) - 自适应边界的textview组件，可自动调整文本大小
- [DanmakuFlameMaster](https://gitee.com/openharmony-sig/DanmakuFlameMaster) - 弹幕放送、解析与绘制库

[返回目录](#目录)

#### <a name="listview-JS_ArkTS"></a>ListView

- [MultiType](https://gitee.com/openharmony-sig/MultiType) - 为list组件创建多种条目类型的库
- [recyclerview-animators](https://gitee.com/openharmony-sig/recyclerview-animators) - 带有添加删除动画效果以及整体动画效果的list组件库
- [overscroll-decor](https://gitee.com/openharmony-sig/overscroll-decor) - UI滚动组件

[返回目录](#目录)

#### <a name="indicator-JS_ArkTS"></a>Indicator

- [CircleIndicator](https://gitee.com/openharmony-sig/CircleIndicator) - 指示器归一化组件，能力类似java组件CircleIndicator，MagicIndicator，ViewPagerIndicator等库
- [MaterialTabIndicator](https://gitee.com/nishanttrivedi/MaterialTabIndicator) - This library is developed to provide Tab-Bars with different icon types and different bar types.

[返回目录](#目录)

#### <a name="pageslider-JS_ArkTS"></a>PageSlider

- [RecyclerViewPager](https://gitee.com/openharmony-sig/RecyclerViewPager) - 支持无限循环、左右翻页切换效果、上下翻页切换效果、Material风格的容器
- [ohos-CoverflowJS](https://gitee.com/openharmony-tpc/ohos-CoverflowJS) - 轮播图自定义组件

[返回目录](#目录)

#### <a name="progressbar-JS_ArkTS"></a>ProgressBar

- [MaterialProgressBar](https://gitee.com/openharmony-sig/MaterialProgressBar) - 自定义进度条显示效果的归一化组件，能力类似java组件MaterialProgressBar，materialish-progress，SmoothProgressBar等库
- [ArcProgressStackViewJS](https://gitee.com/openharmony-tpc/ArcProgressStackViewJS) - 弧形模式下显示进度条

[返回目录](#目录)

#### <a name="dialog-JS_ArkTS"></a>Dialog

- [material-dialogs](https://gitee.com/openharmony-sig/material-dialogs) - 自定义弹框组件
- [search_dialogJS](https://gitee.com/openharmony-tpc/search_dialogJS) - 搜索Dialog

[返回目录](#目录)

#### <a name="layout-JS_ArkTS"></a>Layout

- [vlayout](https://gitee.com/openharmony-sig/vlayout) - 布局扩展组件，提供一整套布局方案和布局间的组件复用功能
- [TextLayoutBuilder](https://gitee.com/openharmony-sig/TextLayoutBuilder) - 文本自定义布局组件
- [ohos-SwipeLayout](https://gitee.com/openharmony-sig/ohos-SwipeLayout) - 各种样式的滑动组件
- [SmartRefreshLayout](https://gitee.com/hihopeorg/SmartRefreshLayout) - 一个强大，稳定，成熟的下拉刷新框架，并集成各种的炫酷、多样、实用、美观的Header,

[返回目录](#目录)

#### <a name="chips-JS_ArkTS"></a>Chips

- [MaterialChips](https://gitee.com/applibgroup/MaterialChips) - This library is developed to provide different types of chips

[返回目录](#目录)

#### <a name="tab-菜单切换JS_ArkTS"></a>Tab-菜单切换

- [navigation-bar](https://gitee.com/hihopeorg/navigation-bar) - 自定义导航栏组件

[返回目录](#目录)

#### <a name="选择器JS_ArkTS"></a>选择器

- [ohos-PickerView](https://gitee.com/openharmony-sig/ohos-PickerView) - 选择器，包括时间选择、地区选择、分割线设置、文字大小颜色设置
- [WheelPicker](https://gitee.com/openharmony-sig/WheelPicker) - 滚轮选择器

[返回目录](#目录)

#### <a name="其他ui-自定义控件JS_ArkTS"></a>其他UI-自定义控件

- [ohos-MPChart](https://gitee.com/openharmony-sig/ohos-MPChart) - 图表归一化组件，能力类似java组件AndroidMPChart，arhartengine等库
- [RefreshLoadMoreComponentJS](https://gitee.com/openharmony-tpc/RefreshLoadMoreComponentJS) - 下拉刷新控件
- [PullToRefresh](https://gitee.com/openharmony-sig/PullToRefresh) - 支持设置内置动画的各种属性，支持设置自定义动画的下拉刷新、上拉加载组件
- [SelectViewJS](https://gitee.com/openharmony-tpc/SelectViewJS) - 自定义选择组件，提供了本地查询和自动排序功能
- [StatusViewJS](https://gitee.com/openharmony-tpc/StatusViewJS) - 自定义不同状态组件
- [Image3DJs](https://gitee.com/openharmony-tpc/Image3DJs) - 根据监听手机传感器实现裸眼3D效果的控件
- [JsComponent](https://gitee.com/luzhen050/JsComponent) - 实现了水波纹、滚轮、浮动列表、表单等组件
- [Sheet](https://gitee.com/rekyone/sheet) - 基于 Canvas 实现的高性能 Excel 表格引擎组件 OpenHarmonySheet
- [OpenHarmany-Pretty-Weather](https://gitee.com/qiezisoftware/openharmany-pretty-weather) - 一个基于 OpenHarmony 下的 JavaScript 天气组件
- [CurtainJs](https://gitee.com/openharmony-tpc/CurtainJs) - 高亮显示局部区域
- [Neumorphism_Smarthome_Darkmode](https://github.com/applibgroup/Neumorphism_Smarthome_Darkmode) - Neumorphism library is an UI component which supports neumorphic effects
- [Neumorphism_Smart_Watch](https://github.com/applibgroup/Neumorphism_Smart_Watch) - Neumorphism library is an UI component which supports neumorphic effects
- [Neumorphism_Living-Room_Design](https://github.com/applibgroup/Neumorphism_Living-Room_Design) - Neumorphism library is an UI component which supports neumorphic effects
- [Neumorphism_Smarthome_Lightmode](https://github.com/applibgroup/Neumorphism_Smarthome_Lightmode) - Neumorphism library is an UI component which supports neumorphic effects
- [Alert](https://github.com/applibgroup/Alert) - Neumorphism library is an UI component which supports neumorphic effects
- [Buttons](https://github.com/applibgroup/Buttons) - Neumorphism library is an UI component which supports neumorphic effects
- [Card](https://github.com/applibgroup/Card) - Neumorphism library is an UI component which supports neumorphic effects
- [Checkbox](https://github.com/applibgroup/Checkbox) - Neumorphism library is an UI component which supports neumorphic effects
- [Dropdown](https://github.com/applibgroup/Dropdown) - Neumorphism library is an UI component which supports neumorphic effects
- [form](https://github.com/applibgroup/form) - Neumorphism library is an UI component which supports neumorphic effects
- [Navbar](https://github.com/applibgroup/Navbar) - Neumorphism library is an UI component which supports neumorphic effects
- [pagination](https://github.com/applibgroup/pagination) - Neumorphism library is an UI component which supports neumorphic effects
- [progress](https://github.com/applibgroup/progress) - Neumorphism library is an UI component which supports neumorphic effects
- [Theme](https://github.com/applibgroup/Theme) - Neumorphism library is an UI component which supports neumorphic effects
- [Verbal_Expressions](https://github.com/applibgroup/Verbal_Expressions) - Verbal Expressions is a Javascript library that helps construct difficult regular expressions.
- [MaterialSliders](https://gitee.com/BibekLakra/MaterialSliders) - MaterialSliders is a slider library in TS which provides general purpose sliders having custom UI.

[返回目录](#目录)

### <a name="动画JS_ArkTS"></a>动画

#### <a name="动画JS_ArkTS"></a>动画

- :tw-1f195: [jbox2d](https://gitee.com/hihopeorg/jbox2d) - 实现C++ 物理引擎LiquidFun和Box2d的端口，可用于游戏开发，使物体的运动更加真实，让游戏场景看起来更具交互性，比如愤怒的小鸟
- [lottieETS](https://gitee.com/openharmony-tpc/lottieETS) - 适用于OpenHarmony的动画库,功能类似于Java组件lottie，AndroidViewAnimations，Leonids等库。
- [shimmer-ohos](https://gitee.com/openharmony-sig/shimmer-ohos) - 提供各种形态的页面加载的闪烁效果
- [rebound](https://gitee.com/openharmony-tpc/rebound) - 用于模拟弹簧动力学以驱动物理动画库
- [LoadingViewJs](https://gitee.com/openharmony-tpc/LoadingViewJs) - 多种漂亮样式的加载动画

[返回目录](#目录)

### <a name="图片JS_ArkTS"></a>图片

#### <a name="图片加载JS_ArkTS"></a>图片加载

- :tw-1f195: [ohos-gif-drawable](https://gitee.com/openharmony-sig/ohos-gif-drawable) - 本项目是OpenHarmony系统的一款GIF图像渲染库，基于Canvas进行绘制，支持gif图片相关功能
- [ImageKnife](https://gitee.com/openharmony-tpc/ImageKnife) - 更高效、更轻便、更简单的图像加载缓存库，能力类似java组件glide、disklrucache、glide-transformations、fresco、picasso、uCrop、Luban、pngj、Android-Image-Cropper、android-crop等库
- [ImageViewZoom](https://gitee.com/openharmony-sig/ImageViewZoom) - 图片加载组件，支持缩放和平移

[返回目录](#目录)

#### <a name="图片处理JS_ArkTS"></a>图片处理

- [LargeImage](https://gitee.com/openharmony-tpc/LargeImage) - 加载可以执行缩放（放大和缩小）和滚动操作的图像
- [XmlGraphicsBatik](https://gitee.com/openharmony-tpc/XmlGraphicsBatik) - 用于处理可缩放矢量图形（SVG）格式的图像，例如显示、生成、解析或者操作图像
- [ohos-svg](https://gitee.com/openharmony-sig/ohos-svg) - SVG解析器
- [we-cropper](https://gitee.com/hihopeorg/we-cropper) - canvas图片裁剪器
- [JsImagePreview](https://gitee.com/thoseyears/js-image-preview) - 图片预览组件，包含水波纹动画、跳转动画以及相关手势

[返回目录](#目录)

### <a name="多媒体JS_ArkTS"></a>多媒体

- :tw-1f195: [ijkplayer](https://gitee.com/openharmony-sig/ijkplayer) - 一款基于FFmpeg的视频播放器
- [mp4parser](https://gitee.com/openharmony-tpc/mp4parser) - 一个读取、写入操作音视频文件编辑的工具
- [mp3agic](https://gitee.com/openharmony-sig/mp3agic) - mp3文件ID3标签处理库
- [metadata-extractor](https://gitee.com/hihopeorg/metadata-extractor) - 用于从图像、视频和音频文件中提取 Exif、IPTC、XMP、ICC 和其他元数据的组件

[返回目录](#目录)

### <a name="文件数据JS_ArkTS"></a>文件数据

#### <a name="数据库JS_ArkTS"></a>数据库

- [greenDAO](https://gitee.com/openharmony-sig/greenDAO) - 数据库能力归一化组件，能力类似java组件greenDAO，DBFlow，android-database-sqlcipher，ormlit-core，ormlite-android等库

[返回目录](#目录)

#### <a name="数据存储JS_ArkTS"></a>数据存储

- :tw-1f195: [DiskLruCache](https://gitee.com/openharmony-sig/ohos_disklrucache) - 专门为OpenHarmony打造的一款磁盘缓存库，通过LRU算法进行磁盘数据存取。
- [MMKV](https://gitee.com/openharmony-tpc/MMKV) - 一款小型键值对存储框架

[返回目录](#目录)

#### <a name="数据封装传递JS_ArkTS"></a>数据封装传递

- :tw-1f195: [mqtt](https://gitee.com/openharmony-sig/ohos_mqtt) - 使应用程序能够连接到MQTT代理以发布消息、订阅主题和接收发布的消息。
- [LiveEventBus](https://gitee.com/openharmony-sig/LiveEventBus) - 消息总线，支持Sticky，支持跨进程，支持跨应用广播

[返回目录](#目录)

#### <a name="文件解析编码解码JS_ArkTS"></a>文件解析编码解码

- :tw-1f195: [fileio-extra](https://gitee.com/openharmony-sig/fileio-extra) - 提供了更丰富全面的文件操作功能
- :tw-1f195: [pdfViewer](https://gitee.com/openharmony-tpc/pdfViewer) - 用于解析和展示PDF
- :tw-1f195: [gson](https://gitee.com/openharmony-sig/gson) - Gson用于对象与JSON字符串之间的互相转换，并支持JsonElement对象类型，使JSON字符串与对象之间的转换更高效、灵活，并且易于使用。
- :tw-1f195: [unrar](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/unrar) - unrar用于解压rar格式文件的库。
- [protobuf](https://gitee.com/openharmony-tpc/protobuf) - 序列化和反序列化，能力类似java组件protobuf，libprotobuf-mutator等库
- [okio](https://gitee.com/openharmony-tpc/okio) - 一个通过数据流、序列化和文件系统为系统输入和输出提供支持的库
- [jtar](https://gitee.com/openharmony-sig/jtar) - 提供了一种使用IO流创建和读取 tar 文件方法的库
- [commonmark](https://gitee.com/openharmony-tpc/commonmark) - 高度可扩展的 Markdown 解析器
- [CommonsCompress](https://gitee.com/openharmony-tpc/CommonsCompress) - 压缩/解压功能组件，能力类似java组件common-compress，zip4j，aircompressor，7zip等库
- [commons-codec](https://gitee.com/openharmony-tpc/commons-codec) - 一个包含各种格式的简单编码器和解码器
- [juniversalchardet](https://gitee.com/openharmony-sig/juniversalchardet) - 字符编码识别组件
- [snakeyaml](https://gitee.com/openharmony-sig/snakeyaml) - YAML文件解析器
- [base64](https://gitee.com/openharmony-sig/base64) - base64编解码器
- [htmltoxml](https://gitee.com/openharmony-sig/jsoup/tree/master/htmltoxml) - HTML转换成整洁的XHTML
- [commons-cli](https://gitee.com/openharmony-sig/commons-cli) - 该库用于解析传递给程序的命令行选项
- [brotli](https://gitee.com/openharmony-sig/brotli) - Brotli 是一种通用无损压缩算法
- [dd-plist](https://gitee.com/openharmony-sig/dd-plist) - plist文件解析库
- [avro](https://gitee.com/hihopeorg/avro) - Avro是一个数据序列化的系统，可以将数据结构或对象转化成便于存储或传输的格式，适合于远程或本地大规模数据的存储和交换
- [epublib](https://gitee.com/hihopeorg/epublib) - 是一个用于读取/写入/操作epub文件的库

[返回目录](#目录)

### <a name="网络-JS_ArkTS"></a>网络

#### <a name="网络JS_ArkTS"></a>网络

- :tw-1f195: [axios](https://gitee.com/openharmony-sig/axios) - 一个基于 promise 的网络请求库，可以运行 node.js 和浏览器中。本库基于Axios 原库进行适配，使其可以运行在 OpenHarmony，并沿用其现有用法和特性。
- :tw-1f195: [smbj](https://gitee.com/hihopeorg/smbj) - 主要用于计算机间共享文件，支持安全保护，访问共享目录、打开文件、读写文件等。
- :tw-1f195: [jackrabbit](https://gitee.com/hihopeorg/jackrabbit) - Jackrabbit是支持AMQP（Advanced Message Queuing Protocol）网络通信协议的库，可以在一个进程间传递异步消息。
- [httpclient](https://gitee.com/openharmony-tpc/httpclient) - 一个默认高效的 HTTP 客户端，能力类似java组件okhttp、legacy、chuck、android-async-http、httpclient、netty、AutobahnAndroid、OkGo等库的功能特性
- [retrofit](https://gitee.com/openharmony-tpc/retrofit) - 一款类型安全的 HTTP 客户端
- [okdownload](https://gitee.com/openharmony-sig/okdownload) - 文件下载工具
- [mars](https://gitee.com/hihopeorg/mars) - 跨平台网络组件
- [RocketChat](https://gitee.com/openharmony-tpc/RocketChat) - 服务器方法和消息流订阅的应用程序接口
- [commons-fileupload](https://gitee.com/openharmony-sig/commons-fileupload) - 用来做文件上传（支持分片）、基本请求、文件下载
- [stun-server](https://gitee.com/hihopeorg/stun-server) - 是基于STUN协议的服务开源组件，它允许客户端获取NAT分配的外部IP地址和端口号，还可以识别NAT的行为类型
- [smack](https://gitee.com/hihopeorg/smack) - 是一个基于XMPP协议的一个聊天客户端

[返回目录](#目录)

### <a name="安全-JS_ArkTS"></a>安全

#### <a name="加密算法JS_ArkTS"></a>加密算法

- :tw-1f195: [jwks-rsa](https://gitee.com/hihopeorg/jwks-rsa) - 用于从 JWKS（JSON Web 密钥集）端点检索RSA签名密钥的库，支持所有当前注册的 JWK 类型和 JWS 算法
- [checksum](https://gitee.com/openharmony-sig/checksum) - 计算散列函数的组件，如sha1，MD5等
- [crypto-js](https://gitee.com/openharmony-sig/crypto-js) - 加密算法类库，目前支持MD5、SHA-1、SHA-256、HMAC、HMAC-MD5、HMAC-SHA1、HMAC-SHA256、PBKDF2等
- [jama](https://gitee.com/openharmony-sig/jama) - 基本线性代数包，它提供了用于构造和操作真实密集矩阵的用户级类，各种构造函数从双精度浮点数的二维数组创建矩阵
- [jchardet](https://gitee.com/openharmony-sig/jchardet) - 自动字符集检测算法
- [is-png](https://gitee.com/openharmony-sig/is-png) - 判断是否是png格式文件的库
- [is-webp](https://gitee.com/openharmony-sig/is-webp) - 判断是否是webp的库

[返回目录](#目录)

### <a name="工具JS_ArkTS"></a>工具

#### <a name="相机-相册JS_ArkTS"></a>相机-相册

- [zxing](https://gitee.com/openharmony-tpc/zxing) - 一个解析/生成二维码的组件，能力类似java组件zxing，Zbar、zxing-android-embedded、BGAQRCode-Android等
- [qr-code-generator](https://gitee.com/openharmony-sig/qr-code-generator) - 二维码生成器

[返回目录](#目录)

#### <a name="日志JS_ArkTS"></a>日志

- [logback](https://gitee.com/openharmony-sig/logback) - 日志组件，支持打印与保存，能力类似java组件logback，commons-logging，minlog，slf4j等库

[返回目录](#目录)

#### <a name="蓝牙工具JS_ArkTS"></a>蓝牙工具

- [FastBle](https://gitee.com/openharmony-sig/FastBle) - 蓝牙能力集成工具，支持过滤，扫描，链接，读取，写入
- [ohos-beacon-library](https://gitee.com/openharmony-sig/ohos-beacon-library) - 应用与蓝牙信标交互组件

[返回目录](#目录)

### <a name="其他JS_ArkTS"></a>其他

#### <a name="其他JS_ArkTS"></a>其他
- :tw-1f195: [openharmony-polyfill](https://gitee.com/openharmony-sig/openharmony-polyfill) - 该项目是一个polyfill，包含NodeJs所有模块的子集以及一款webpack插件。用于 npm 仓中 nodejs build-in 基础模块的 api 适配，通过 webpack 插件形式集成到Openharmony的SDK中，避免原 npm 库在鸿蒙平台上使用时做侵入式修改。
- :tw-1f195: [appauth-js](https://gitee.com/hihopeorg/appauth-js) - 用于公共客户端与OAuth 2.0 和OpenID Connect提供者进行通信，支持 OAuth 的PKCE 扩展，主要为应用程序提供身份验证和授权
- [VCard](https://gitee.com/openharmony-tpc/VCard) - 电子名片的文件格式标准
- [flexsearch-ohos](https://gitee.com/openharmony-tpc/flexsearch-ohos) - 最快且最具内存灵活性的全文搜索库
- [thrift](https://gitee.com/openharmony-tpc/thrift) - 一个轻量级的、独立于语言的软件堆栈，用于点对点RPC实现
- [pinyin4js](https://gitee.com/openharmony-tpc/pinyin4js) - 一个功能强大的拼音库，能力类似java组件pinyin4j，TinyPinyin等库
- [arouter-api-onActivityResult](https://gitee.com/openharmony-tpc/arouter-api-onActivityResult) - 用于在各种应用或页面间的跳转和页面间的数据传递
- [mixpanel-ohos](https://gitee.com/openharmony-tpc/mixpanel-ohos) - 一种可捕获有关用户如何与数字产品交互的数据产品分析工具
- [xutils](https://gitee.com/openharmony-sig/xutils) - 网络、文件、数据库操作工具集
- [Hamcrest](https://gitee.com/openharmony-sig/Hamcrest) - 单元测试框架
- [jmustache](https://gitee.com/openharmony-sig/jmustache) - Mustache 模板语言的js实现
- [ohos_mail](https://gitee.com/openharmony-sig/ohos_mail) - 检测并解析MIME 格式的电子邮件消息流， 并构建电子邮件消息的组件合集。能力类似java组件jakarta-mail、mime4j、mime-types等库

[返回目录](#目录)

## <a name="三方组件C_CPP"></a>三方组件C_CPP

### <a name="工具类C_CPP"></a>工具类

#### <a name="音视频C_CPP"></a>音视频
- [vorbis](https://gitee.com/openharmony-sig/vorbis) - 一种通用音频和音乐编码格式组件
- [opus](https://gitee.com/openharmony-sig/opus) - Opus是一个开放格式的有损声音编码格式
- [flac](https://gitee.com/openharmony-sig/flac) - 无损音频编解码器

[返回目录](#目录)

#### <a name="加解密算法C_CPP"></a>加解密算法
- [libogg](https://gitee.com/openharmony-sig/libogg) - 编解码器
- [libsodium](https://gitee.com/hihopeorg/libsodium) - 易用，可移植的加解密库

[返回目录](#目录)

#### <a name="图像处理C_CPP"></a>图像处理
- [stb-image](https://gitee.com/openharmony-sig/stb-image) - C/C++实现的图像解码库
- [pyclipper](https://gitee.com/openharmony-sig/pyclipper) - 图形处理库，可以用于解决平面二维图形的多边形简化、布尔运算和偏置处理
- [jbig2enc](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/jbig2enc) -  是JBIG2文件的编码器
- [leptonica](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/leptonica) -  一个开放源码的C语言库，它被广泛地运用于图像处理和图像分析
- [openjpeg](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/openjpeg) -  是用 C 语言编写的开源 JPEG 2000 编解码器
- [libtiff](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/libtiff) -  是一个用来读写标签图片(tiff)的库。该库还支持如下文件格式的转化

[返回目录](#目录)

#### <a name="网络通信C_CPP"></a>网络通信
- [nanopb](https://gitee.com/hihopeorg/nanopb) - 轻量的支持C语言的一种数据协议，可用于数据存储、通信协议等方面
- [c-ares](https://gitee.com/openharmony-sig/c-ares) - 异步解析器库，适用于需要无阻塞地执行 DNS 查询或需要并行执行多个 DNS 查询的应用程序
- [libevent](https://gitee.com/openharmony-sig/libevent) - 事件通知库
- [kcp](https://gitee.com/openharmony-sig/kcp) - ARQ 协议,可解决在网络拥堵情况下tcp协议的网络速度慢的问题

[返回目录](#目录)

#### <a name="数据压缩C_CPP"></a>数据压缩

- [lzma](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/lzma)  \- 是2001年以来得到发展的一个数据压缩算法，它是一种高压缩比的传统数据压缩软件 
- [zstd](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/zstd) -  一种快速的无损压缩算法，是针对 zlib 级别的实时压缩方案，以及更好的压缩比 
- [minizip-ng](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/minizip-ng) \- 一个用C编写的zip文件操作库 

[返回目录](#目录)

#### <a name="XML解析C_CPP"></a>XML解析

- [xerces-c](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/xerces-c) - 一个开放源代码的XML语法分析器，它提供了SAX和DOM API

[返回目录](#目录)

#### <a name="编码转换C_CPP"></a>编码转换

- [iconv](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/iconv) - 一个实现字符集转换的库，用于没有Unicode或无法从其他字符转换为Unicode的系统

[返回目录](#目录)

#### <a name="其他工具类C_CPP"></a>其他工具类

- [lua](https://gitee.com/openharmony-sig/lua) - Lua是一种功能强大、高效、轻量级、可嵌入的脚本语言
- [inotify-tools](https://gitee.com/hihopeorg/inotify-tools) - 异步文件系统监控组件，它满足各种各样的文件监控需要，可以监控文件系统的访问属性、读写属性、权限属性、删除创建、移动等操作
- [libharu](https://gitee.com/openharmony-sig/libharu) - 用于生成 PDF格式的文件
- [leveldb](https://gitee.com/openharmony-sig/leveldb) - 快速键值存储库，提供从字符串键到字符串值的有序映射

[返回目录](#目录)

